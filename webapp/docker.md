docker build --platform=linux/amd64 --tag webapp .
docker run --env-file env.list -p 80:80 webapp
docker tag webapp:latest deathiotb7/webapp
docker login
docker push deathiotb7/webapp
