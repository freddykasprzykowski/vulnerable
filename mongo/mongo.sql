use admin
db.createUser({user: "records",pwd: "supersecurepassword",roles: [ { role: "dbOwner", db: "records" } ]})
use records
db.user.insert({name: "Ada Lovelace", age: 205})
# https://www.rfc-editor.org/rfc/rfc5737
mongodb://records:supersecurepassword@203.0.113.55:27017
