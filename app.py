#!/usr/bin/env python3
# Copyright Freddy Kasprzykowski
# SPDX-License-Identifier: MIT-0


from aws_cdk import (
    App,
    Stack,
)
from stack import InsecureStack

app = App()

insecure_stack = InsecureStack(app, "InsecureStack")

app.synth()
