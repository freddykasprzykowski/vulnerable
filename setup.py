# Copyright Freddy Kasprzykowski
# SPDX-License-Identifier: MIT-0
import setuptools

AWS_CDK_VERSION = "2.60.0"

with open("README.md") as fp:
    long_description = fp.read()


setuptools.setup(
    name="insecure",
    version="0",
    description="Insecure Stack",
    long_description=long_description,
    long_description_content_type="text/markdown",

    author="Freddy Kasprzykowski",

    package_dir={"": "."},
    packages=setuptools.find_packages(where="cdk"),

    install_requires=[
        "aws-cdk-lib==" + AWS_CDK_VERSION,
    ],

    python_requires=">=3.10",

    classifiers=[
        "Development Status :: Beta",
        "Programming Language :: JavaScript",
        "Programming Language :: Python :: 3.10",
    ],
)