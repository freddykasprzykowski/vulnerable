# Insecure Kubernetes and AWS Services for vulnerability assessment

## Steps
* deploy CDK app
* configure mongodb running in the EC2 instance using `mongo.sql`
* follow `eks/eks-steps.md` to create EKS cluster and deploy `webapp`

## Security stuff

### AWS Service security
* `0.0.0.0/0` inbound security group
* overpermissive EC2 instance role
* old version of MongoDB
* old version of Ubuntu AMI

### Cluster security
- [CIS benchmarks](https://www.cisecurity.org/benchmark/kubernetes)

### POD security
- [Pod Security Admission](https://kubernetes.io/docs/concepts/security/pod-security-admission/)
  - enforce, audit, warn
  - [Pod Security Standards](https://kubernetes.io/docs/concepts/security/pod-security-standards/)
- [Pod Security Context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/)
  - runAsUser, runAsGroup, fsGroup, readOnlyRootFileSystem
- [Sysdig article](https://sysdig.com/blog/kubernetes-admission-controllers/)

### Tools
- checkov - static analysis
- falco - rules engine
- sysdig - system calls
- audit logs - kube-apiserver logs
- kubesec - resource security posture
- prometheus - k8s monitoring