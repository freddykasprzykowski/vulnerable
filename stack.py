import aws_cdk as cdk
from aws_cdk import (
    aws_s3 as s3,
    aws_ec2 as ec2,
    aws_iam as iam,
    CfnOutput as _output,
)


class InsecureStack(cdk.Stack):

    def __init__(self, scope: cdk.App, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        public_bucket = s3.Bucket(
            self,
            "MongoDBBackups",
            public_read_access=True,
        )

        vpc_subnets = [
            ec2.SubnetConfiguration(
                subnet_type=ec2.SubnetType.PUBLIC,
                name="Public",
                cidr_mask=24
            ),
            ec2.SubnetConfiguration(
                subnet_type=ec2.SubnetType.PRIVATE_WITH_EGRESS,
                name="Private",
                cidr_mask=24
            )
        ]

        cidr = "192.168.0.0/16"

        vpc_cidr = ec2.IpAddresses.cidr(cidr)

        vpc = ec2.Vpc(
            self,
            "VPC",
            ip_addresses=vpc_cidr,
            max_azs=2,
            subnet_configuration=vpc_subnets,
            nat_gateways=1,
        )

        security_group = ec2.SecurityGroup(
            self,
            "SecurityGroup",
            vpc=vpc,
            description="Security Group",
        )

        security_group.add_ingress_rule(
            connection=ec2.Port.all_traffic(),
            description="allow all traffic from VPC CIDR",
            peer=ec2.Peer.ipv4(cidr),
        )

        security_group.add_ingress_rule(
            connection=ec2.Port.tcp(22),
            description="allow SSH TCP/22",
            peer=ec2.Peer.ipv4("0.0.0.0/0"),
        )

        security_group.add_ingress_rule(
            connection=ec2.Port.tcp(27017),
            description="allow MongoDB TCP/27017",
            peer=ec2.Peer.ipv4("0.0.0.0/0"),
        )

        user_data = ec2.UserData.for_linux()

        with open("mongo/user_data.sh") as f:
            commands = f.read()

        user_data.add_commands(
           commands,
        )

        instance_role = iam.Role(
            self,
            "InstanceRole",
            assumed_by=iam.ServicePrincipal("ec2.amazonaws.com"),
        )

        instance_role_policy = iam.PolicyStatement(
            sid="S3Star",
            effect=iam.Effect.ALLOW,
            actions=["s3:*"],
            resources=["*"],
        )

        instance_role.add_to_policy(instance_role_policy)

        # ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-20210928
        ami_id = "ami-0b0ea68c435eb488d"
        ami_map = {"us-east-1": ami_id}

        outdated_ubuntu_linux = ec2.MachineImage.generic_linux(
            ami_map=ami_map,
            user_data=user_data,
        )

        outdated_ubuntu_instance = ec2.Instance(
            self,
            "OutdatedUbuntuInstance",
            allow_all_outbound=True,
            instance_name="MongoDB4.4",
            instance_type=ec2.InstanceType("t2.micro"),
            machine_image=outdated_ubuntu_linux,
            vpc=vpc,
            vpc_subnets=ec2.SubnetSelection(
                subnet_type=ec2.SubnetType.PUBLIC
            ),
            security_group=security_group,
            key_name="mongodb",
            instance_role=instance_role,
        )




