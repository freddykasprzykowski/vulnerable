### install kubectl in host (cloudshell or local)
- `brew install kubectl`
- `kubectl version`

### install eksctl in host (cloudshell or local)
- `brew install eksctl`
- `eksctl version`

### deploy cluster
- update eks/insecure-cluster.yaml with private subnets from the VPC
- `eksctl create cluster -f eks/insecure-cluster.yaml`
- master role configured -> aws eks update-kubeconfig --name insecure-cluster --region us-east-1 --role-arn arn:aws:iam::083760711385:role/eksctl-insecure-cluster-nodegroup-NodeInstanceRole-XXXXXXXXXX

### verify cluster
- `eksctl get cluster --name=insecure-cluster --region=us-east-1`
- `kubectl get nodes -o wide`
- `kubectl get pods -A -o wide`
- `kubectl get service`

### deploy node.js+mongodb stack (previously tested with minikube)
- update webapp/k8s/mongo-config.yaml with private IP address from mongodb instance
- `kubectl apply -f webapp/k8s/mongo-config.yaml`
- `kubectl get configmap`
- `kubectl describe configmap mongo-config`
- `kubectl apply -f webapp/k8s/mongo-secret.yaml`
- `kubectl get secrets`
- `kubectl describe secrets mongo-secret`
- ssh to mongo instance: use mongo/mongod.conf and mongo/mongo.sql
- `ssh -i keys/mongodb.pem ubuntu@203.0.113.55`
- `kubectl apply -f webapp/k8s/webapp.yaml`
### check deployment
- `kubectl get pods -o wide`
- `kubectl describe pods webapp-deployment-cc44db98c-ckbsn`
- `kubectl describe service webapp-loadbalancer`
- navigate to URL
### tear down
- `eksctl delete cluster --name insecure-cluster`
- `npx cdk destroy`


